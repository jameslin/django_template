from .base import Base
import os
from configurations import values


class Production(Base):
    DEBUG = False

    DATABASES = {
        'default': {
            'ENGINE': os.environ.get('DJANGO_DB_ENGINE', 'django.db.backends.mysql'),
            'NAME': os.environ.get('DJANGO_DB_NAME', '{{ project_name }}'),
            'USER': os.environ.get('DJANGO_DB_USER', '{{ project_name }}'),
            'PASSWORD': os.environ.get('DJANGO_DB_PASSWORD'),
        }
    }

    ADMINS = [('James Lin', 'james@lin.net.nz')]
    ALLOWED_HOSTS = ['*']
    MANDRILL_API_KEY = values.Value('')
    EMAIL_BACKEND = "djrill.mail.backends.djrill.DjrillBackend"
    DEFAULT_FROM_EMAIL = values.Value('{{ project_name }} Server<noreply@{{ project_name }}.co.nz>')
