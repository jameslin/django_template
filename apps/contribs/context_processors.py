# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.conf import settings


def common_settings(request):
    return {'CACHE_BREAKER': settings.CACHE_BREAKER}
